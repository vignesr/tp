package com.ajc.springtraining;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(AppConfig.class, JPAConfig.class);
		context.refresh();

		remplirTables();
		Best_3_Notes(2016);
		Best_3_Notes(2017);
		Best_3_Notes(2018);

	}

	public static void remplirTables() {
		java.sql.Connection conn = null;
		PreparedStatement stmt = null;

		try {

			ApplicationContext appContext = (ApplicationContext) new ClassPathXmlApplicationContext("beans.xml");
			DataSource datasource = (DataSource) appContext.getBean("datasource2");
			conn = datasource.getConnection();
			final String INSERT_EMPLOYEE_QUERY = "INSERT INTO `formationtp`.`employee` (`employee_id`, `email`, `login`, `nom`, `password`, `prenom`, `role`)"
					+ " VALUES ('1', 'email1', 'login1', 'nom1', 'password1', 'prenom1', 'role1'),"
					+ "('2', 'email2', 'login2', 'nom2', 'password2', 'prenom2', 'role2'),"
					+ "('3', 'email3', 'login3', 'nom3', 'password3', 'prenom3', 'role3'),"
					+ "('4', 'email4', 'login4', 'nom4', 'password4', 'prenom4', 'role4'),"
					+ "('5', 'email5', 'login5', 'nom5', 'password5', 'prenom5', 'role5'),"
					+ "('6', 'email6', 'login6', 'nom6', 'password6', 'prenom6', 'role6')";

			final String INSERT_ADRESSE_QUERY = "INSERT INTO `formationtp`.`address` (`address_id`, `code_postal`, `numero_rue`, `pays`, `rue`, `ville`, `employeeID`)"
					+ " VALUES ('1', 'code1', '1', 'pays1', 'rue1', 'ville1', '1'),"
					+ "('2', 'code2', '2', 'pays2', 'rue2', 'ville2', '1'),"
					+ "('3', 'code3', '3', 'pays3', 'rue3', 'ville3', '2'),"
					+ "('4', 'code4', '4', 'pays4', 'rue4', 'ville4', '3'),"
					+ "('5', 'code5', '5', 'pays5', 'rue5', 'ville5', '4'),"
					+ "('6', 'code6', '6', 'pays6', 'rue6', 'ville6', '4'),"
					+ "('7', 'code7', '7', 'pays7', 'rue7', 'ville7', '5'),"
					+ "('8', 'code8', '8', 'pays8', 'rue8', 'ville8', '6')";

			final String INSERT_NOTES_QUERY = "INSERT INTO `formationtp`.`notes` (`note_id`, `assiduite`, `performance`, `social`, `sport`, `year`, `employeeID`)"
					+ " VALUES ('1', '2', '3', '4', '5', '2016', '1')," + "('2', '4', '5', '6', '4', '2017', '1'),"
					+ "('3', '5', '5', '6', '7', '2018', '1')," + "('4', '0', '1', '6', '1', '2016', '2'),"
					+ "('5', '5', '0', '6', '7', '2017', '2')," + "('6', '5', '5', '0', '3', '2018', '2'),"
					+ "('7', '1', '5', '6', '7', '2016', '3')," + "('8', '5', '1', '1', '9', '2017', '3'),"
					+ "('9', '9', '5', '6', '7', '2018', '3')," + "('10', '5', '5', '2', '3', '2016', '4'),"
					+ "('11', '5', '5', '6', '7', '2017', '4')," + "('12', '2', '5', '6', '5', '2018', '4'),"
					+ "('13', '4', '5', '5', '7', '2016', '5')," + "('14', '5', '2', '6', '1', '2017', '5'),"
					+ "('15', '5', '3', '6', '7', '2018', '5')," + "('16', '3', '5', '6', '4', '2016', '6'),"
					+ "('17', '5', '3', '6', '7', '2017', '6')," + "('18', '4', '5', '8', '2', '2018', '6')";

			stmt = conn.prepareStatement(INSERT_EMPLOYEE_QUERY);
			stmt.executeUpdate();
			stmt = conn.prepareStatement(INSERT_ADRESSE_QUERY);
			stmt.executeUpdate();
			stmt = conn.prepareStatement(INSERT_NOTES_QUERY);
			stmt.executeUpdate();

			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void Best_3_Notes(int year) {

		java.sql.Connection conn = null;
		PreparedStatement stmt = null;

		try {

			ApplicationContext appContext = (ApplicationContext) new ClassPathXmlApplicationContext("beans.xml");
			DataSource datasource = (DataSource) appContext.getBean("datasource2");
			conn = datasource.getConnection();

			final String BEST_3_QUERY = "SELECT notes.employeeID, (notes.assiduite+notes.performance+notes.social+notes.sport)/4 as moyenne "
					+ "FROM formationtp.notes " + "WHERE year = ? " + "ORDER BY moyenne DESC LIMIT 3;";

			stmt = conn.prepareStatement(BEST_3_QUERY);
			stmt.setInt(1, year);

			ResultSet rs = null;
			rs = stmt.executeQuery();

			
			ArrayList<Integer> employeeIdlist = new ArrayList<Integer>();
			ArrayList<Double> employeeMoyenneList = new ArrayList<Double>();

			while (rs.next()) {
				employeeIdlist.add(rs.getInt("employeeID"));
				employeeMoyenneList.add(rs.getDouble("moyenne"));
			}

			final String BEST_3_EMPLOYEES = "SELECT prenom, nom FROM employee WHERE id IN (?,?,?);";

			stmt = conn.prepareStatement(BEST_3_EMPLOYEES);
			stmt.setInt(1, employeeIdlist.get(0));
			stmt.setInt(2, employeeIdlist.get(1));
			stmt.setInt(3, employeeIdlist.get(2));

			rs = stmt.executeQuery();

			System.out.println("Les 3 meilleurs employés pour l'année " + year + " sont :\n");

			int i = 0;
			while (rs.next()) {
				String prenom = rs.getString("prenom");
				String nom = rs.getString("nom");
				System.out.println(prenom + " " + nom + " avec la moyenne : " + employeeMoyenneList.get(i++));
			}
			
			System.out.println("\n");

		}

		catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
