package com.ajc.springtraining;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.ajc.springtraining")
public class AppConfig {

}
