package com.ajc.springtraining.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.ajc.springtraining")
public class BeanConfig {

}
