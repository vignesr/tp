package com.ajc.springtraining.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.ajc.springtraining.dao.interfaces.IAdresseDao;
import com.ajc.springtraining.models.Adresse;

@Component
@Scope("singleton")
public class AdresseDAO implements IAdresseDao{

	public void saveAdresse(Adresse adresse, int employee_id) {
		
		java.sql.Connection conn = null;
		PreparedStatement stmt = null;

		try {

			int id = 0;
			final String SELECT_MAX_QUERY = "SELECT MAX(ADDRESSE_ID) FROM ADDRESSE;";
			ApplicationContext appContext = (ApplicationContext) new ClassPathXmlApplicationContext("beans.xml");
			DataSource datasource = (DataSource) appContext.getBean("datasource2");
			conn = datasource.getConnection();
			stmt = conn.prepareStatement(SELECT_MAX_QUERY);
			ResultSet rs = null;
			rs = stmt.executeQuery();
			if (rs.next()) {
				id = rs.getInt("MAX(ADDRESSE_ID)");
			}
			
			System.out.println("id :" + id);

			final String INSERT_QUERY = "INSERT INTO ADDRESSE (  ADDRESSE_ID, NUMERO_RUE, RUE, CODE_POSTALE, VILLE, PAYS, EMPLOYEE_ID)"
					+ "                               VALUES (           ?,          ?,   ?,           ?,     ?,    ?,           ?);";
			stmt = conn.prepareStatement(INSERT_QUERY);
			stmt.setInt(1, id+1);
			stmt.setInt(2, adresse.getNumero_rue());
			stmt.setString(3, adresse.getRue());
			stmt.setString(4, adresse.getCode_postale());
			stmt.setString(5, adresse.getVille());
			stmt.setString(6, adresse.getPays());
			stmt.setInt(7, employee_id);
			
			stmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public Adresse getAdressebyId(int id) {
		
		return null;
	}

	public void saveAdressebyJdbcTemplate(Adresse adresse) {
		
		
	}

	public Adresse getAdresseByJdbcTemplate(int id) {
		
		return null;
	}

	
	

}
