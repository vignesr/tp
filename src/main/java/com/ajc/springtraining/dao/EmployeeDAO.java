package com.ajc.springtraining.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.ajc.springtraining.dao.interfaces.IEmployeDao;
import com.ajc.springtraining.models.Employee;
import com.ajc.springtraining.repo.EmployeeRepository;

@Component
@Scope("singleton")
public class EmployeeDAO implements IEmployeDao {

	public void saveEmployee(Employee employee) {
		java.sql.Connection conn = null;
		PreparedStatement stmt = null;

		try {

			/*int id = 0;
			final String SELECT_MAX_QUERY = "SELECT MAX(ID) FROM EMPLOYEE;";*/
			ApplicationContext appContext = (ApplicationContext) new ClassPathXmlApplicationContext("beans.xml");
			DataSource datasource = (DataSource) appContext.getBean("datasource2");
			conn = datasource.getConnection();
			/*stmt = conn.prepareStatement(SELECT_MAX_QUERY);
			ResultSet rs = null;
			rs = stmt.executeQuery();
			if (rs.next()) {
				id = rs.getInt("MAX(ID)");
			}
			
			System.out.println("id :" + id);*/

			final String INSERT_QUERY = "INSERT INTO EMPLOYEE ( LOGIN, EMAIL, PASSWORD, PRENOM, NOM, ROLE)"
					+ "                                VALUES (     ?,     ?,        ?,      ?,   ?,    ?);";
			stmt = conn.prepareStatement(INSERT_QUERY);
			stmt.setString(1, employee.getLogin());
			stmt.setString(2, employee.getEmail());
			stmt.setString(3, employee.getPassword());
			stmt.setString(4, employee.getPrenom());
			stmt.setString(5, employee.getNom());
			stmt.setString(6, employee.getRole());
			stmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			

	}

	public Employee getEmployebyId(int id) {

		java.sql.Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Employee employee = new Employee();
		try {

			final String SELECT_QUERY = "SELECT ID, LOGIN, EMAIL, PASSWORD, PRENOM, NOM, ROLE FROM EMPLOYEE WHERE ID = ?";
			ApplicationContext appContext = (ApplicationContext) new ClassPathXmlApplicationContext("beans.xml");
			DataSource datasource = (DataSource) appContext.getBean("datasource2");
			conn = datasource.getConnection();
			stmt = conn.prepareStatement(SELECT_QUERY);
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if (rs.next()) {
				employee.setId(rs.getInt("ID"));
				employee.setLogin(rs.getString("LOGIN"));
				employee.setPrenom(rs.getString("PRENOM"));
				employee.setNom(rs.getString("NOM"));
				employee.setPassword(rs.getString("PASSWORD"));
				employee.setEmail(rs.getString("EMAIL"));
				employee.setRole(rs.getString("ROLE"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employee;

	}

	public void saveEmployebyJdbcTemplate(Employee employe) {

	}

	public Employee getEmployeByJdbcTemplate(int id) {
		return null;
	}

	

}
