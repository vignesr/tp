package com.ajc.springtraining.dao.interfaces;


import com.ajc.springtraining.models.Adresse;

public interface IAdresseDao {
	
	public void saveAdresse(Adresse adresse, int employee_id);
	public Adresse getAdressebyId(int id);
	public void saveAdressebyJdbcTemplate(Adresse adresse);
	public Adresse getAdresseByJdbcTemplate (int id);

}
