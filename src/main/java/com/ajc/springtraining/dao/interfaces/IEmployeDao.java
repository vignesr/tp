package com.ajc.springtraining.dao.interfaces;

import com.ajc.springtraining.models.Employee;

public interface IEmployeDao {
	public void saveEmployee(Employee employee);
	public Employee getEmployebyId(int id);
	public void saveEmployebyJdbcTemplate(Employee employe);
	public Employee getEmployeByJdbcTemplate (int id);

}
