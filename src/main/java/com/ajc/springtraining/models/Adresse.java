package com.ajc.springtraining.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="address")
public class Adresse {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="address_id")
	
	private int id;
	private int numero_rue;
	private String rue;
	private String code_postal;
	private String ville;
	private String pays;
		
	
	@ManyToOne(
			cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}
	)
	@JoinColumn(name="employeeID",
				referencedColumnName = "employee_id")
	private Employee employee;
	
	public Adresse(int numero_rue, String rue, String code_postale, String ville, String pays) {
		super();
		this.numero_rue = numero_rue;
		this.rue = rue;
		this.code_postal = code_postale;
		this.ville = ville;
		this.pays = pays;
		
	}
	public int getId() {
		return id;
	}
	public void setId(int address_id) {
		this.id = address_id;
	}
	public int getNumero_rue() {
		return numero_rue;
	}
	public void setNumero_rue(int numero_rue) {
		this.numero_rue = numero_rue;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public String getCode_postale() {
		return code_postal;
	}
	public void setCode_postale(String code_postale) {
		this.code_postal = code_postale;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	
	
	
}
