package com.ajc.springtraining.models;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="notes")
public class Notes {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="note_id")
	private int id;
	
	@ManyToOne(
			cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}
	)
	@JoinColumn(name="employeeID",
				referencedColumnName = "employee_id")
	private Employee employee;
	private int year;
	private int sport;
	private int social;
	private int performance;
	private int assiduite;
	
	
	
	
	
	
	public Notes(Employee employee, int year, int sport, int social, int performance, int assiduite) {
		super();
		this.employee = employee;
		this.year = year;
		this.sport = sport;
		this.social = social;
		this.performance = performance;
		this.assiduite = assiduite;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getSport() {
		return sport;
	}
	public void setSport(int sport) {
		this.sport = sport;
	}
	public int getSocial() {
		return social;
	}
	public void setSocial(int social) {
		this.social = social;
	}
	public int getPerformance() {
		return performance;
	}
	public void setPerformance(int performance) {
		this.performance = performance;
	}
	public int getAssiduite() {
		return assiduite;
	}
	public void setAssiduite(int assiduite) {
		this.assiduite = assiduite;
	}
	
	
}


